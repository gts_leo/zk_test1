package com.gothic;

import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.ZkClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ZkServerClient {

    //存放服务器端集群
    public static List<String> listServer = new ArrayList<String>();

    public static void main(String[] args) {
        initServer();
        ZkServerClient client = new ZkServerClient();
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String name;
            try {
                name = console.readLine();
                if ("exit".equals(name)) {
                    System.exit(0);
                }
                client.send(name);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // 注册所有server
    public static void initServer() {
        listServer.clear();
        //1.创建zk连接
        final ZkClient zkClient = new ZkClient("127.0.0.1:2181", 5000, 10000);
        //2.读取注册到节点上的信息
        final String root = "/test";
        final List<String> childrens = zkClient.getChildren(root);
        //3.获取子节点值
        for (String node : childrens) {
            //子节点路径
            String path = root + "/" + node;
            //获取子节点的值
            String value = zkClient.readData(path);
            //将子节点值放置在集合
            listServer.add(value);
        }
        System.out.println("服务发现成功 ：" + listServer.toString());
        serverCount = listServer.size();
        //使用zk事件监听,如果服务宕机，重新读取新的节点
        zkClient.subscribeChildChanges(root, new IZkChildListener() {
            public void handleChildChange(String parentPath, List<String> currentChilds) throws Exception {
                System.out.println("有服务器数量有变化时，重新获取服务器的节点信息");
                listServer.clear();
                for (String node : currentChilds) {
                    //子节点路径
                    String path = root + "/" + node;
                    //获取子节点的值
                    String value = zkClient.readData(path);
                    //将子节点值放置在集合
                    listServer.add(value);
                }
                serverCount = listServer.size();
                System.out.println("服务发现成功 ：" + listServer.toString());
            }
        });

    }

    //请求总数
    private static int reqCount = 1;
    //服务器总数
    private static int serverCount = 0;

    // 获取当前server信息
    public static String getServer() {
        //本地轮询算法
        String serverName = listServer.get(reqCount % serverCount);
        System.out.println("客户端请求次数： " + reqCount + "  对应服务器： " + serverName);
        reqCount++;
        return serverName;
    }

    public void send(String name) {

        String server = ZkServerClient.getServer();
        String[] cfg = server.split(":");

        Socket socket = null;
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            socket = new Socket(cfg[0], Integer.parseInt(cfg[1]));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            out.println(name);
            while (true) {
                String resp = in.readLine();
                if (resp == null)
                    break;
                else if (resp.length() > 0) {
                    System.out.println("Receive : " + resp);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
