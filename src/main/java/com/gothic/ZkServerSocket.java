package com.gothic;

import org.I0Itec.zkclient.ZkClient;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ZkServerSocket implements Runnable {
    private static int port = 8082;

    public static void main(String[] args) throws IOException {
        ZkServerSocket server = new ZkServerSocket(port);
        Thread thread = new Thread(server);
        thread.start();
    }

    public ZkServerSocket(int port) {
        this.port = port;
    }

    public void run() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            registServer();
            System.out.println("服务启动成功，端口为:" + port);
            Socket socket = null;
            while (true) {
                socket = serverSocket.accept();
                new Thread(new ServerHandler(socket)).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (serverSocket != null) {
                    serverSocket.close();
                }
            } catch (Exception e2) {

            }
        }
    }

    //注册服务
    public void registServer() {
        //1.创建zk连接
        ZkClient zkClient = new ZkClient("127.0.0.1:2181", 5000, 10000);
        //2.创建父节点
        String root = "/test";
        //3.如果父节点不存在，创建父节点
        if (!zkClient.exists(root)) {
            //创建一个持久节点
            zkClient.createPersistent(root);
        }
        //4.创建子节点
        String nodeName = root + "/service_" + port;
        String nodeValue = "127.0.0.1:" + port;
        //如果子节点存在则删除子节点
        if (zkClient.exists(nodeName)) {
            zkClient.delete(nodeName);
        }
        //创建一个临时的子节点,类似于dubbo中的服务发现
        zkClient.createEphemeral(nodeName, nodeValue);
        System.out.println("服务注册成功： " + nodeValue);
    }
}
